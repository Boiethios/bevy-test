use bevy::{
    input::{
        mouse::{MouseButtonInput, MouseWheel},
        ElementState,
    },
    prelude::*,
};
use std::time::{Duration, Instant};

pub struct CameraPlugin;
impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<MapZoomScaleRes>()
            .add_event::<MapDraggedEvent>()
            .add_event::<MapClickedEvent>()
            .add_startup_system(init_camera_system)
            .add_system(custom_mouse_event_system)
            .add_system(drag_map_system)
            .add_system(click_map_system)
            .add_system(zoom_map_system);
    }
}

#[derive(Component)]
struct MapCamera;

struct MapDraggedEvent {
    delta: Vec2,
}

pub struct MapClickedEvent {
    /// The point where clicked. Todo: give the tile that has been clicked instead.
    point: Vec2,
}

/// Represent how the map is zoomed in/out. Span from -5 to 5.
#[derive(Copy, Clone)]
struct MapZoomScaleRes(i32);

fn init_camera_system(mut commands: Commands) {
    commands
        .spawn_bundle(OrthographicCameraBundle::new_2d())
        .insert(MapCamera);
}

/// Reacts to the `MapDraggedEvent` event.
fn drag_map_system(
    mut map_dragged_events: EventReader<MapDraggedEvent>,
    mut cameras: Query<&mut Transform, With<MapCamera>>,
) {
    let mut camera = cameras.single_mut();
    if let Some(event) = map_dragged_events.iter().next_back() {
        camera.translation = Vec3::new(event.delta.x, event.delta.y, camera.translation.z);
    }
}

/// Reacts to the `MapDraggedEvent` event.
fn click_map_system(mut map_dragged_events: EventReader<MapClickedEvent>) {
    if let Some(event) = map_dragged_events.iter().next_back() {
        println!("Clicked at {:?}", event.point);
    }
}

/// Changes the map zoom level when a wheel event is generated.
fn zoom_map_system(
    mut map_zoom_scale: ResMut<MapZoomScaleRes>,
    mut mouse_wheel_events: EventReader<MouseWheel>,
    mut cameras: Query<&mut Transform, With<MapCamera>>,
) {
    let mut camera = cameras.single_mut();
    let wheel_delta = mouse_wheel_events.iter().map(|event| event.y as i32).sum();

    map_zoom_scale.zoom(wheel_delta);
    camera.scale = Vec3::new(
        1. * map_zoom_scale.to_zoom_factor(),
        1. * map_zoom_scale.to_zoom_factor(),
        1.,
    );
}

impl Default for MapZoomScaleRes {
    fn default() -> Self {
        MapZoomScaleRes(0)
    }
}

impl MapZoomScaleRes {
    const MIN: i32 = -5;
    const MAX: i32 = 5;

    fn to_zoom_factor(self) -> f32 {
        match self.0.signum() {
            1 => 1.2_f32.powi(self.0),
            -1 => 0.8_f32.powi(-self.0),
            _0 => 1.,
        }
    }

    fn zoom(&mut self, factor: i32) {
        self.0 = (self.0 - factor).min(Self::MAX).max(Self::MIN)
    }
}

/* Drag or click event generator */

/// A resource needed to keep in track where the user has pressed the left button.
struct ClickedPointRes {
    /// The camera translation when clicked.
    camera_origin: Vec2,
    /// The window coordinates where clicked.
    window_origin: Vec2,
    event: EventType,
}

enum EventType {
    /// The dragging event must be sent.
    Dragged,
    /// When the user has clicked. After 500 ms, it initiates a drag event.
    NotDragged(Instant),
}

/// Generates the `MapDraggedEvent` or `MapClickedEvent` when needed.
fn custom_mouse_event_system(
    time: Res<Time>,
    window: Res<Windows>,
    map_zoom_scale: Res<MapZoomScaleRes>,
    mut clicked_point: Local<Option<ClickedPointRes>>,
    cameras: Query<&Transform, With<MapCamera>>,
    mut mouse_button_input_events: EventReader<MouseButtonInput>,
    mut map_dragged_events: EventWriter<MapDraggedEvent>,
    mut map_clicked_events: EventWriter<MapClickedEvent>,
) {
    let window = window.get_primary().unwrap();
    for event in mouse_button_input_events.iter() {
        match (event.button, event.state) {
            (MouseButton::Left, ElementState::Pressed) => {
                let camera = cameras.single();
                *clicked_point = window
                    .cursor_position()
                    .map(|window_origin| ClickedPointRes {
                        camera_origin: camera.translation.truncate(),
                        window_origin,
                        event: EventType::NotDragged(time.last_update().unwrap()),
                    })
            }
            (MouseButton::Left, ElementState::Released) => {
                if let Some(ClickedPointRes {
                    camera_origin: _,
                    window_origin,
                    event: EventType::NotDragged(_),
                }) = *clicked_point
                {
                    map_clicked_events.send(MapClickedEvent {
                        point: window_origin,
                    });
                }
                *clicked_point = None;
            }
            _ => {}
        }
    }

    if let Some(point) = &mut *clicked_point {
        let cursor_position = window.cursor_position().unwrap();
        match point.event {
            EventType::Dragged => map_dragged_events.send(MapDraggedEvent {
                delta: point.camera_origin
                    - (cursor_position - point.window_origin) * map_zoom_scale.to_zoom_factor(),
            }),
            EventType::NotDragged(instant) => {
                if time.last_update().unwrap() - instant > Duration::from_millis(500)
                    || point.window_origin.distance(cursor_position) > 10.
                {
                    point.event = EventType::Dragged;
                }
            }
        }
    }
}
